# Project tree

```bash
└── src
    ├── assets 
    ├── app   ,
    │   ├── core  
    │   │   ├── models
    │   │   └── services    
    │   ├── register 
    │   ├── shared 
    │   │   ├── components 
    │   │   ├── constants
    │   │   └── functions
    │   └── store
    │   │   ├── actions
    │   │   ├── effects
    │   │   ├── meta-reducers
    │   │   ├── reducers
    │   │   └── selectors 
    ├── environments
    └── styles 
```

# Run Development server

- Run `npm install` in the rootDir to instal dependencies.
- Run `ng serve` for a dev server.
- Navigate to `http://localhost:4200/`.

# Possible optimizations

- Optimize the registration view buttons' function, this buttons are created to help user navigate between steps and save data in local storage. I think that it was possible to create a generic function to do so.
- Enhance the scss structure
- Improve the Dynamic Icon (I have detected a time gap when this component is initialized).

# Things that coud be done better

- A much efficient solution for CORS issue, because mine works in the development server but not in the production server. ( [The link to my deployed application](https://wunder-coding-challenge.vercel.app/register) )
- Add a dark mode
- Add more data validation and data format.

# My deployed application

As mentioned before the [deployed application](https://wunder-coding-challenge.vercel.app/register) works fine but needs a fix for CORS issue.