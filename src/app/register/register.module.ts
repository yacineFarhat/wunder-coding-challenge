import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { environment } from '@env';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import * as fromEffects from '@store/effects';
import { metaReducers, reducers } from '@store/state';
import { CoreModule } from './../core/core.module';
import { SharedModule } from './../shared/shared.module';
import { RegisterRoutingModule } from './register-routing.module';
import { RegisterComponent } from './register.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    RegisterRoutingModule,
    SharedModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    EffectsModule.forRoot([...fromEffects.effects]),
  ],
  declarations: [RegisterComponent],
})
export class RegisterModule {}
