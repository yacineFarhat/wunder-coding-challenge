import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { PaymentCard, User, View } from '@core/models';
import { select, Store } from '@ngrx/store';
import { APP_LABEL, STEPPER_WIDTH } from '@shared/constants';
import { getRandomInt } from '@shared/functions';
import { next, previous, reset } from '@store/actions/user.actions';
import { selectUser } from '@store/selectors/user.selectors';
import { State } from '@store/state';
import { AnimationOptions } from 'ngx-lottie';
import { Observable } from 'rxjs';
import { savePayment } from './../store/actions/user.actions';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  currentStep: number = 1;
  formElement: View[] = [];
  user: User = {} as User;
  form: FormGroup = {} as FormGroup;
  error: boolean = false;
  options: AnimationOptions = {} as AnimationOptions;

  user$: Observable<User> = new Observable<User>();

  constructor(private fb: FormBuilder, private store: Store<State>) {
    this.formElement = [
      {
        step: 1,
        inputs: [
          {
            id: 'firstName',
            label: APP_LABEL.user.firstName.label,
            placeholder: APP_LABEL.user.firstName.placeholder,
            type: 'text',
            autofocus: true,
            required: true,
            onFocus: () => this.hideError(),
          },
          {
            id: 'lastName',
            label: APP_LABEL.user.lastName.label,
            placeholder: APP_LABEL.user.lastName.placeholder,
            type: 'text',
            required: true,
            onFocus: () => this.hideError(),
          },
          {
            id: 'telephone',
            label: APP_LABEL.user.telephone.label,
            placeholder: APP_LABEL.user.telephone.placeholder,
            type: 'tel',
            required: true,
            onFocus: () => this.hideError(),
          },
        ],
        buttons: [
          {
            title: APP_LABEL.button.next,
            onClick: () => this.goToSecondStep(),
          },
        ],
      },
      {
        step: 2,
        inputs: [
          {
            id: 'houseNumber',
            label: APP_LABEL.user.address.houseNumber.label,
            placeholder: APP_LABEL.user.address.houseNumber.placeholder,
            type: 'number',
            formGroupName: 'address',
            autofocus: true,
            required: true,
            onFocus: () => this.hideError(),
          },
          {
            id: 'street',
            label: APP_LABEL.user.address.street.label,
            placeholder: APP_LABEL.user.address.street.placeholder,
            type: 'text',
            formGroupName: 'address',
            required: true,
            onFocus: () => this.hideError(),
          },
          {
            id: 'city',
            label: APP_LABEL.user.address.city.label,
            placeholder: APP_LABEL.user.address.city.placeholder,
            type: 'tel',
            formGroupName: 'address',
            required: true,
            onFocus: () => this.hideError(),
          },
          {
            id: 'zipCode',
            label: APP_LABEL.user.address.zipCode.label,
            placeholder: APP_LABEL.user.address.zipCode.placeholder,
            type: 'number',
            formGroupName: 'address',
            required: true,
            onFocus: () => this.hideError(),
          },
        ],
        buttons: [
          {
            title: APP_LABEL.button.previous,
            outline: true,
            onClick: () => this.goBack(),
          },
          {
            title: APP_LABEL.button.next,
            onClick: () => this.goToThirdStep(),
          },
        ],
      },
      {
        step: 3,
        inputs: [
          {
            id: 'accountOwner',
            label: APP_LABEL.user.accountOwner.label,
            placeholder: APP_LABEL.user.accountOwner.placeholder,
            type: 'text',
            autofocus: true,
            required: true,
            onFocus: () => this.hideError(),
          },
          {
            id: 'iban',
            label: APP_LABEL.user.iban.label,
            placeholder: APP_LABEL.user.iban.placeholder,
            type: 'text',
            required: true,
            onFocus: () => this.hideError(),
          },
        ],
        buttons: [
          {
            title: APP_LABEL.button.previous,
            outline: true,
            onClick: () => this.goBack(),
          },
          {
            title: APP_LABEL.button.next,
            onClick: () => this.goToFourthStep(),
          },
        ],
      },
      {
        step: 4,
        buttons: [
          {
            title: APP_LABEL.button.reset,
            onClick: () => this.reset(),
          },
        ],
      },
    ];
  }

  get stepperWidth(): string {
    return STEPPER_WIDTH[this.currentStep];
  }

  get formView(): View | undefined {
    return this.formElement.find((view) => view.step === this.currentStep);
  }

  get f() {
    return this.form.controls;
  }

  get paymentErrorMessage() {
    return this.user.error
      ? 'Sorry an error occurred! Please try again!'
      : 'Congratulations ! You have successfully registered and your Payment Data is = ' +
          this.user.paymentDataId;
  }

  ngOnInit() {
    this.user$ = this.store.pipe(select(selectUser));
    this.user$.subscribe((user) => {
      if (user) {
        this.user = user;
        this.options = {
          path: `../../../../assets/icon/${
            this.user.error ? 'warning' : 'success'
          }.json`,
          loop: false,
        };
        this.currentStep = user.currentStep;
      }
    });
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      firstName: [this.user.firstName],
      lastName: [this.user.lastName],
      telephone: [this.user.telephone],
      address: this.fb.group({
        street: [this.user.address?.city],
        houseNumber: [this.user.address?.houseNumber],
        zipCode: [this.user.address?.zipCode],
        city: [this.user.address?.city],
      }),
      iban: [this.user.iban],
      accountOwner: [this.user.accountOwner],
    });
  }

  hideError() {
    this.error = false;
  }

  goToSecondStep() {
    const user: User = this.form.value;
    if (
      user.firstName?.length &&
      user.lastName?.length &&
      user.telephone?.length
    ) {
      user.id = getRandomInt();
      user.currentStep = this.currentStep + 1;
      this.store.dispatch(next({ user }));
    } else {
      this.error = true;
    }
  }

  goToThirdStep() {
    const user: User = this.form.value;
    if (
      user.address?.city?.length &&
      user.address?.zipCode?.length &&
      user.address?.houseNumber?.length &&
      user.address?.street?.length
    ) {
      user.currentStep = this.currentStep + 1;
      this.store.dispatch(next({ user }));
    } else {
      this.error = true;
    }
  }

  goToFourthStep() {
    const user: User = this.form.value;
    if (user?.iban?.length && user.accountOwner?.length) {
      user.currentStep = this.currentStep + 1;
      this.store.dispatch(next({ user }));
      const card = new PaymentCard(this.user.id, user.iban, user.accountOwner);
      this.store.dispatch(savePayment({ card }));
    } else {
      this.error = true;
    }
  }

  goBack() {
    const user: User = this.form.value;
    user.currentStep = this.currentStep - 1;
    this.error = false;
    this.store.dispatch(previous({ user }));
  }

  reset() {
    this.store.dispatch(reset());
    this.createForm();
  }
}
