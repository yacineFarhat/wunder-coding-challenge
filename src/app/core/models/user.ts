import { Address } from './address';

export interface User {
  id: number;
  firstName?: string;
  lastName?: string;
  telephone?: string;
  address?: Address;
  iban?: string;
  accountOwner?: string;
  paymentDataId?: string;
  currentStep: number;
  error: boolean;
}
