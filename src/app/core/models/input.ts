export interface Input {
  id: string;
  label?: string;
  placeholder?: string;
  type?: string;
  formGroupName?: string;
  autofocus?: boolean;
  required?: boolean;
  onBlur?: Function;
  onFocus: Function;
}
