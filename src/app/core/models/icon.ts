export interface Icon {
  height?: string;
  width?: string;
  success?: boolean;
  loop?: boolean;
}
