import { Button } from './button';
import { Icon } from './icon';
import { Input } from './input';

export interface View {
  step: number;
  inputs?: Input[];
  buttons?: Button[];
  icon?: Icon;
  message?: string;
}
