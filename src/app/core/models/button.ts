export interface Button {
  title: string;
  outline?: boolean;
  color?: string;
  onClick: Function;
}
