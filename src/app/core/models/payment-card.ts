export class PaymentCard {
  customerId: number;
  iban: string;
  owner: string;

  constructor(customerId: number, iban: string, owner: string) {
    this.customerId = customerId;
    this.iban = iban;
    this.owner = owner;
  }
}

export interface PaymentCardResponse {
  paymentDataId: string;
}
