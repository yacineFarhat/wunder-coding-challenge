import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PaymentCard, PaymentCardResponse } from '@core/models';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private paymentURL =
    '/default/wunderfleet-recruiting-backend-dev-save-payment-data';

  constructor(private http: HttpClient) {}

  savePayment(card: PaymentCard): Observable<PaymentCardResponse> {
    return this.http.post<PaymentCardResponse>(this.paymentURL, card);
  }
}
