import { LocalStorageService } from './local-storage.service';
import { UserService } from './user.service';

export const services: any[] = [LocalStorageService, UserService];

export * from './local-storage.service';
export * from './user.service';
