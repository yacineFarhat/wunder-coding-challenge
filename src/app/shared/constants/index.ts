import { generateLabel } from '@shared/functions';

export const APP_PREFIX = 'wcc-';

export const APP_NAME = 'app';

export const USER_NAME = 'user';

export const APP_KEY = 'APP';

export const MAX_INT = 100;

export const APP_LABEL = {
  user: {
    firstName: generateLabel('First Name'),
    lastName: generateLabel('Last Name'),
    telephone: generateLabel('Telephone'),
    address: {
      street: generateLabel('Street'),
      houseNumber: generateLabel('House Number'),
      zipCode: generateLabel('Zip Code'),
      city: generateLabel('City'),
    },
    accountOwner: generateLabel('Account Owner'),
    iban: generateLabel('IBAN'),
  },
  button: {
    next: 'Next',
    previous: 'Previous',
    reset: 'Reset',
  },
};

export const STEPPER_WIDTH: { [key: number]: string } = {
  1: '15',
  2: '45',
  3: '75',
  4: '100',
};
