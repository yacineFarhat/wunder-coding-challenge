import { MAX_INT } from './../constants/index';

export function generateLabel(value: string) {
  return {
    label: value,
    placeholder: 'Insert ' + value,
  };
}

export function getRandomInt() {
  return Math.floor(Math.random() * MAX_INT) + 1;
}
