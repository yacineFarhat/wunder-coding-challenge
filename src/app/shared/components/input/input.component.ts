import {
  Component,
  ElementRef,
  EventEmitter,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
})
export class InputComponent {
  @Input() id: string = 'identifier';
  @Input() type: string = 'text';
  @Input() placeholder: string = 'Placeholder';
  @Input() label: string = 'Label';
  @Input() parentForm: FormGroup = {} as FormGroup;
  @Input() groupName: string = '';
  @Input() required: boolean = false;
  @Input() autofocus: boolean = false;

  @Output() onBlur = new EventEmitter<string>();
  @Output() onFocus = new EventEmitter();

  constructor() {}

  get hasFormGroupName(): boolean {
    return this.groupName.length !== 0;
  }

  @ViewChild('input', { static: false })
  set input(element: ElementRef<HTMLInputElement>) {
    if (element && this.autofocus) {
      element.nativeElement.focus();
    }
  }

  update(value: string) {
    this.onBlur.emit(value);
  }
}
