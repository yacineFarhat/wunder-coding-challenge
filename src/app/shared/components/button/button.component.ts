import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
})
export class ButtonComponent {
  @Input() outline: boolean = false;
  @Input() color: string = 'dark';
  @Input() title: string = 'Title';

  @Output() onClick = new EventEmitter();

  constructor() {}
}
