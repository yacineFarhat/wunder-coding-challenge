import { Component, Input } from '@angular/core';
import { AnimationOptions } from 'ngx-lottie';

@Component({
  selector: 'app-dynamic-icon',
  templateUrl: './dynamic-icon.component.html',
  styleUrls: ['./dynamic-icon.component.scss'],
})
export class DynamicIconComponent {
  @Input() height: string = '80';
  @Input() width: string = '80';
  @Input() options: AnimationOptions = {} as AnimationOptions;

  get customHeight(): string {
    return this.height + 'px';
  }

  get customWidth(): string {
    return this.width + 'px';
  }

  constructor() {}
}
