import { ButtonComponent } from './button/button.component';
import { CardComponent } from './card/card.component';
import { DynamicIconComponent } from './dynamic-icon/dynamic-icon.component';
import { InputComponent } from './input/input.component';
import { MessageComponent } from './message/message.component';
import { StepperComponent } from './stepper/stepper.component';

export const components: any[] = [
  ButtonComponent,
  CardComponent,
  DynamicIconComponent,
  InputComponent,
  StepperComponent,
  MessageComponent,
];

export * from './button/button.component';
export * from './card/card.component';
export * from './dynamic-icon/dynamic-icon.component';
export * from './input/input.component';
export * from './message/message.component';
export * from './stepper/stepper.component';
