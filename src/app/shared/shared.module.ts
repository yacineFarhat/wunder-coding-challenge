import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LottieModule } from 'ngx-lottie';
import { CoreModule } from './../core/core.module';
import * as fromComponents from './components';

export function playerFactory() {
  return import('lottie-web');
}

@NgModule({
  declarations: [...fromComponents.components],
  imports: [
    CommonModule,
    CoreModule,
    LottieModule.forRoot({ player: playerFactory }),
  ],
  exports: [...fromComponents.components],
})
export class SharedModule {}
