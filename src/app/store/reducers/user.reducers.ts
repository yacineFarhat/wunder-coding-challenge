import { User } from '@core/models';
import { Action, createReducer, on } from '@ngrx/store';
import { userActions } from '../actions';

export interface State extends User {}

export const initialState: State = {
  id: 0,
  firstName: '',
  lastName: '',
  telephone: '',
  address: { street: '', houseNumber: '', zipCode: '', city: '' },
  paymentDataId: '',
  currentStep: 1,
  error: false,
};

const userReducer = createReducer(
  initialState,
  on(userActions.next, userActions.previous, (state, { user }) => {
    return { ...state, ...user, error: false };
  }),
  on(userActions.reset, () => {
    return initialState;
  }),
  on(userActions.savePaymentSuccess, (state, { card }) => {
    return { ...state, ...{ paymentDataId: card.paymentDataId, error: false } };
  }),
  on(userActions.savePaymentFailure, (state) => {
    return { ...state, error: true };
  })
);

export function reducer(state: State | undefined, action: Action) {
  return userReducer(state, action);
}
