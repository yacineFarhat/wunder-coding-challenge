import { Injectable } from '@angular/core';
import { LocalStorageService, UserService } from '@core/services';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { select, Store } from '@ngrx/store';
import { USER_NAME } from '@shared/constants';
import { selectUser } from '@store/selectors/user.selectors';
import { State } from '@store/state';
import { of } from 'rxjs';
import { catchError, map, mergeMap, tap, withLatestFrom } from 'rxjs/operators';
import { userActions } from '../actions';

@Injectable()
export class UserEffects {
  constructor(
    private actions$: Actions,
    private store: Store<State>,
    private localStorageService: LocalStorageService,
    private userService: UserService
  ) {}

  nextOrPrevious$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(userActions.next, userActions.previous),
        withLatestFrom(this.store.pipe(select(selectUser))),
        tap(([action, user]) => {
          this.localStorageService.setItem(USER_NAME, {
            ...action.user,
            ...user,
          });
        })
      ),
    { dispatch: false }
  );

  savePayment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(userActions.savePayment),
      mergeMap(({ card }) =>
        this.userService.savePayment(card).pipe(
          map((card) => userActions.savePaymentSuccess({ card })),

          catchError(() => of(userActions.savePaymentFailure()))
        )
      )
    )
  );

  savePaymentSuccess$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(userActions.savePaymentSuccess),
        withLatestFrom(this.store.pipe(select(selectUser))),
        tap(([action, user]) => {
          this.localStorageService.setItem(USER_NAME, {
            ...user,
            paymentDataId: action.card.paymentDataId,
          });
        })
      ),
    { dispatch: false }
  );

  savePaymentFailure$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(userActions.savePaymentFailure),
        withLatestFrom(this.store.pipe(select(selectUser))),
        tap(([action, user]) => {
          this.localStorageService.setItem(USER_NAME, {
            ...user,
            error: true,
          });
        })
      ),
    { dispatch: false }
  );

  reset$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(userActions.reset),
        tap((action) => this.localStorageService.removeItem(USER_NAME))
      ),
    { dispatch: false }
  );
}
