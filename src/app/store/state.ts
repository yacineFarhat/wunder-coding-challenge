import { environment } from '@env';
import {
  ActionReducerMap,
  createFeatureSelector,
  MetaReducer,
} from '@ngrx/store';
import { debug, initStateFromLocalStorage } from './meta-reducers';
import { userReducer } from './reducers';

export const metaReducers: MetaReducer<State>[] = [initStateFromLocalStorage];

if (!environment.production) {
  metaReducers.unshift(debug);
}

export interface State {
  user: userReducer.State;
}

export const selectUserState = createFeatureSelector<State, userReducer.State>(
  'user'
);

export const reducers: ActionReducerMap<State> = {
  user: userReducer.reducer,
};
