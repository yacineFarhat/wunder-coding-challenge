import { createSelector } from '@ngrx/store';
import { userReducer } from '../reducers';
import { selectUserState } from './../state';

export const selectUser = createSelector(
  selectUserState,
  (state: userReducer.State) => state
);

export const selectError = createSelector(
  selectUserState,
  (state: userReducer.State) => state.error
);
