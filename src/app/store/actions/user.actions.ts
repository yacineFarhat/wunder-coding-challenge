import { PaymentCard, PaymentCardResponse, User } from '@core/models';
import { createAction, props } from '@ngrx/store';

export const reset = createAction('[User] Reset');

export const next = createAction('[User] Next', props<{ user: User }>());

export const previous = createAction(
  '[User] Previous',
  props<{ user: User }>()
);

export const savePayment = createAction(
  '[User] Save Payment',
  props<{ card: PaymentCard }>()
);

export const savePaymentSuccess = createAction(
  '[User] Save Payment Success',
  props<{ card: PaymentCardResponse }>()
);

export const savePaymentFailure = createAction('[User] Save Payment Failure');
