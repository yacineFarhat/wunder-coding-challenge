import { ActionReducer } from '@ngrx/store';
import { State } from './../state';

export function debug(reducer: ActionReducer<State>): ActionReducer<State> {
  return (state, action) => {
    const newState = reducer(state, action);
    console.log(`[DEBUG] action: ${action.type}`, {
      payload: (action as any).payload,
      oldState: state,
      newState,
    });
    return newState;
  };
}
